package com.jcg.midtronics;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.io.File;
import java.text.SimpleDateFormat;

public class AppInfoActivity extends AppCompatActivity {
    TextView tvAppVersion, tvAppDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_info);

        tvAppVersion = findViewById(R.id.tvAppVersion);
        tvAppDate = findViewById(R.id.tvAppDate);

        String versionInfo = "UNKNOWN";
        String installTime = "";

        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(this.getPackageName(), 0);
            versionInfo = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        installTime = getAppTimeStamp(this.getApplicationContext());

        tvAppVersion.setText(versionInfo);
        tvAppDate.setText(installTime);

    }


    public static String getAppTimeStamp(Context context) {
        String timeStamp = "";

        try {
            ApplicationInfo appInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 0);
            String appFile = appInfo.sourceDir;
            long time = new File(appFile).lastModified();

            SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
            timeStamp = formatter.format(time);

        } catch (Exception e) {

        }

        return timeStamp;

    }
}
