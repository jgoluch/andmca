package com.jcg.midtronics;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

public class CountryDetailActivity extends AppCompatActivity {

    TextView tvCountry, tvCapital, tvPopulation, tvArea, tvRegion, tvSubregion;
    String sTemp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country_detail);

        tvCountry = findViewById(R.id.tvCountryName);
        tvCapital = findViewById(R.id.tvCapital);
        tvPopulation = findViewById(R.id.tvPopulation);
        tvArea = findViewById(R.id.tvArea);
        tvRegion = findViewById(R.id.tvRegion);
        tvSubregion = findViewById(R.id.tvSubregion);

        try {
            sTemp = App.countryJSONObject.getString(getResources().getString(R.string.field_country));
            tvCountry.setText(sTemp);

            sTemp = App.countryJSONObject.getString(getResources().getString(R.string.field_capital));
            tvCapital.setText(sTemp);

            sTemp = App.countryJSONObject.getString(getResources().getString(R.string.field_population));
            tvPopulation.setText(sTemp);

            sTemp = App.countryJSONObject.getString(getResources().getString(R.string.field_area));
            tvArea.setText(sTemp);

            sTemp = App.countryJSONObject.getString(getResources().getString(R.string.field_region));
            tvRegion.setText(sTemp);

            sTemp = App.countryJSONObject.getString(getResources().getString(R.string.field_sub_region));
            tvSubregion.setText(sTemp);

        } catch (JSONException e) {
            throw new RuntimeException("JSONException occurred");
        }



    }
}
