package com.jcg.midtronics;

import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

public class DevInfoActivity extends AppCompatActivity {

    TextView tvSummary, tvDevName;
    StringBuilder text = new StringBuilder();
    String sDevName, sDevEmail, sDevPhone;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dev_info);

        BufferedReader reader = null;
        sDevName = getResources().getString(R.string.dev_name);
        sDevEmail = getResources().getString(R.string.dev_email);
        sDevPhone = getResources().getString(R.string.dev_phone);

        tvSummary = findViewById(R.id.tvDevSummary);
        tvDevName = findViewById(R.id.tvDevName);
        tvDevName.setText(sDevName + "\n" + sDevEmail + "\n" + sDevPhone);

        try {
            reader = new BufferedReader(
                    new InputStreamReader(getAssets().open("jg_resume.txt")));

            // do reading, usually loop until end of file reading
            String mLine;
            while ((mLine = reader.readLine()) != null) {
                text.append(mLine);
                text.append('\n');
            }
        } catch (IOException e) {
            Toast.makeText(getApplicationContext(),"Error reading file!",Toast.LENGTH_LONG).show();
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }


            tvSummary.setMovementMethod(new ScrollingMovementMethod());
            tvSummary.setText((CharSequence) text);

        }

    }
}
