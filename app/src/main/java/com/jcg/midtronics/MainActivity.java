package com.jcg.midtronics;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity {
    public static String[] mCountryNames;
    private ListAdapter mCountriesListApapter;
    private ListView mCountriesList;

    private String mBaseURL = "https://restcountries.eu/rest/v1/name/";

    ProgressDialog pd;
    ConnectivityManager connectivityManager;
    NetworkInfo networkInfo;
    String countrySelected;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        connectivityManager = (ConnectivityManager) getApplicationContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        networkInfo = connectivityManager.getActiveNetworkInfo();


        // Get country name strings
        mCountryNames = getResources().getStringArray(R.array.countries_array);

        mCountriesList = findViewById(R.id.countriesList);

        mCountriesListApapter = new ArrayAdapter<String>(this, R.layout.countries_list_item, mCountryNames);

        mCountriesList.setAdapter(mCountriesListApapter);
        mCountriesList.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position,
                                    long arg3)
            {
                countrySelected = (String)adapter.getItemAtPosition(position);
                Log.i("MIDTRONICS", "Selected country: " + countrySelected );

                if(connectedToNetwork()) {
                    new RetrieveCountryInfo().execute(countrySelected);
                }
                else {
                    Log.i("MIDTRONICS", "No internet connection");
                    Toast.makeText(getApplicationContext(), "Network not connected!", Toast.LENGTH_LONG).show();
                }

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu
        getMenuInflater().inflate(R.menu.options_menu, menu);
        return true;
    }

    @SuppressWarnings("deprecation")
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;

        switch(item.getItemId()) {
            case R.id.action_dev_info:
                intent = new Intent(MainActivity.this, DevInfoActivity.class);
                startActivity(intent);
                break;

            case R.id.action_app_info:
                intent = new Intent(MainActivity.this, AppInfoActivity.class);
                startActivity(intent);
                break;
        }

        return true;
    }


    public boolean connectedToNetwork() {

        networkInfo = connectivityManager.getActiveNetworkInfo();

        return (networkInfo != null && networkInfo.isAvailable() &&
                networkInfo.isConnected());

    }

    public class RetrieveCountryInfo extends AsyncTask<String, Void, Void> {

        Intent intent;
        JSONObject jsonObject;

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(MainActivity.this);
            pd.setTitle("Retrieving country info");
            pd.setMessage("Please wait...");
            pd.setCancelable(false);
            pd.setIndeterminate(true);
            pd.show();
        }


        @Override
        protected Void doInBackground(String... strings) {

            String sURL = strings[0];

            try{

                //App.countryJSONObject = getJSONObjectFromURL(mBaseURL + sURL);
                jsonObject = getJSONObjectFromURL(mBaseURL + sURL);

            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (pd!=null) {
                pd.dismiss();
            }

            if(jsonObject != null) {
                App.countryJSONObject = jsonObject;

                Log.i("MIDTRONICS", "Country info retrieved");
                // Go to country detail activity...
                // JSON data available in app class object
                intent = new Intent(MainActivity.this, CountryDetailActivity.class);
                startActivity(intent);
            }
            else {
                Log.i("MIDTRONICS", "Country info unavailable");
                Toast.makeText(getApplicationContext(), "Country details unavailable for " + countrySelected, Toast.LENGTH_LONG).show();
            }

        }
    }


    public static JSONObject getJSONObjectFromURL(String urlString) throws IOException, JSONException {

        int sbLength;
        String goodString;

        HttpURLConnection urlConnection = null;
        URL url = new URL(urlString);
        urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod("GET");
        urlConnection.setReadTimeout(10000 /* milliseconds */ );
        urlConnection.setConnectTimeout(15000 /* milliseconds */ );
        urlConnection.setDoOutput(true);
        //urlConnection.connect();

        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
        StringBuilder sb = new StringBuilder();

        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line + "\n");
        }
        br.close();

        String jsonString = sb.toString();

        /*******************************************
         *  Fix sb to remove '[' and ']' from beginning and end, otherwise can't use string
         *  to create JSONObject
         */
        sbLength = jsonString.length();
        goodString = jsonString.substring(1,(sbLength-2));

//        System.out.println("JSON: " + jsonString);
//        System.out.println("GOOD JSON: " + goodString);

        return new JSONObject(goodString);
    }
}
